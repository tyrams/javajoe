package za.co.javajoe.constants;

/**
 * AUTHOR: Thabo Matjuda
 * DATE: 26 January 2015
 * DESCRIPTION:
 */
public enum StringType {

    Standard, Extended, FilePath, Email, ListString, General,
    Numeric, Alpha, AlphaNumeric, Base64, Delimiter, KeyValue, DateFormat

}
