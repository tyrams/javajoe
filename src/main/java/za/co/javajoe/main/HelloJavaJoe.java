package za.co.javajoe.main;

/**
 *
 *  @ENGINEER:      Mack Makgatho
 *  @DATE:          25 June 2014
 *  @DESCRIPTION:   This class simply returns a string message or a NullPointer
 *                  Exception depending on what arguments you pass it.
 *  @Exexute:       Run the methods in HelloJavaJoeTest class to test it.
 *
 */
public class HelloJavaJoe {

    public String sayHello( String name) {

        String message = "Ola, ";

        // Catering for testCase HelloJavaJoeTest class
        if( name.equals( null)) {
            return name;
        }

        return message + name;
    }

         //jst checking if i can push regards MAck

    //this method just adds numbes in an array
    public int addNumbers(int numbers[]){
        int sum=0;

        for(int number : numbers){

            sum+=number;
        }

        return sum;
    }

}
