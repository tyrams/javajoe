package za.co.javajoe.oop.inheritance;

/**
 * Created with IntelliJ IDEA.
 * User: letladi
 * Date: 2014/07/02
 * Time: 11:42 AM
 * To change this template use File | Settings | File Templates.
 */
public class checkForInheritance {


    classA objecta = new classA();
    classB objectb = new classB();
    classC objectc = new classC();

    /*
      this method just explains the fact that variables are followed but methods are overridden
      the object of class a is the super class of b,that is why a=b is not giving an error,b=a
      will require an explicit cast,a.m1() will call the method in object b which is classB,but a.variable_one will refer
      to the variable of object a which is classA,thats why output is: this is method in classB,firstVriableforA
     */
    public String firstCheck(){

        objecta=objectb;
        String objectAMeth =objecta.m1();
        String objectVb=objecta.variable_one;
        System.out.println(objectAMeth+","+objectVb);
        return objectAMeth+","+objectVb;
    }



}
