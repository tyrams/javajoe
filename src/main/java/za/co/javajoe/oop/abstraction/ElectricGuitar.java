package za.co.javajoe.oop.abstraction;

/**
 * Created by TLOU on 2014/07/01.
 */
public class ElectricGuitar extends StringedInstrument{

    // Instantiating a default Constructor
    public ElectricGuitar(){
        super();
        this.name = "Electric";
        this.numberOfStrings = 6;
    }

    // Instantiating an Overloaded Constructor
    public ElectricGuitar(int noOfStrings){
        super();
        this.name = "Electric";
        this.numberOfStrings = noOfStrings;
    }

    @Override
    public String play() {
        return  "A " + numberOfStrings + " String " + name + " Guitar is Awesome!!!";
    }
}
