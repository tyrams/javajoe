package za.co.javajoe.oop.abstraction;

/**
 * Created by TLOU on 2014/07/01.
 */
public abstract class Instrument {

  /* making this variable protected because we want
     who ever extends this class to be able to access it. */
    protected String name;

    public abstract String play();
}
