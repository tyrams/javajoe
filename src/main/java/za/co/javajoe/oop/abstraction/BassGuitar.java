package za.co.javajoe.oop.abstraction;

/**
 * Created by TLOU on 2014/07/01.
 */
public class BassGuitar extends StringedInstrument{

    // Instantiating a default Constructor
    public BassGuitar() {
        super();
        this.name = "Bass";
        this.numberOfStrings = 4;
    }

    // Instantiating an overloaded Constructor
    public BassGuitar(int noOfStrings){
        super();
        this.name = "Bass";
        this.numberOfStrings = noOfStrings;
    }

    @Override
    public String play() {
        System.out.print("bassssss");
        return  "A " + numberOfStrings + " String " + name + " Guitar is Awesome!!!";
    }
}
