package za.co.javajoe.keys;

/**
 * Created by thabom on 14 August 2014.
 */
public enum AgentDetailsFileKeys {

    AGENT_DETAILS_LIST,
    AGENT_RESULT_FILE_PATH,
    AGENT_DETAILS_FILE_NAME
    ;

}
