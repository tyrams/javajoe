package za.co.javajoe.domain;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: letladi
 * Date: 2014/07/04
 * Time: 2:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class Account implements Serializable {

    private String accountID;
    private String accountNumber;

    public String getAccountID() {
        return accountID;
    }

    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }







}
