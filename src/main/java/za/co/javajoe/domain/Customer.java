package za.co.javajoe.domain;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: letladi
 * Date: 2014/07/04
 * Time: 2:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class Customer implements Serializable {

    private String id;
    private String name;
    private String surName;
    private String accountID;


    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccountID() {
        return accountID;
    }

    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
