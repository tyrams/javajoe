package za.co.javajoe.domain;

import org.apache.commons.lang3.Validate;

import java.io.Serializable;
import java.util.Properties;

/**
 * AUTHOR: Thabo Matjuda
 * DATE: 12 August 2014
 * DESCRIPTION:
 */
public class AgentDetails implements Serializable {


    //Properties
    private String bankAccountNo;
    private String transactionDescription;
    private String userReferenceNo;
    private String transactionAmount;
    private String businessDate;
    private String agentID;
    private String agentIdentification;
    private String agentName;
    private String agentAccountNumber;

    /**
     * No-Arg Constructor
     */
    public AgentDetails(){}

    /**
     * Constructor with some arguments that will be mapped to the properties
     * @param bankAccountNo
     * @param transactionDescription
     */
    public AgentDetails( String bankAccountNo,
                         String transactionDescription){

        this.bankAccountNo = bankAccountNo;
        this.transactionDescription = transactionDescription;

    }

    /**
     * * Constructor with some arguments that will be mapped to the properties
     * @param bankAccountNo
     * @param transactionDescription
     * @param userReferenceNo
     * @param transactionAmount
     * @param businessDate
     * @param agentID
     * @param agentIdentification
     * @param agentName
     * @param agentAccountNumber
     */
    public AgentDetails( String bankAccountNo, String transactionDescription, String userReferenceNo,
                        String transactionAmount, String businessDate, String agentID, String agentIdentification,
                        String agentName, String agentAccountNumber) {

        this.bankAccountNo = bankAccountNo;
        this.transactionDescription = transactionDescription;
        this.userReferenceNo = userReferenceNo;
        this.transactionAmount = transactionAmount;
        this.businessDate = businessDate;
        this.agentID = agentID;
        this.agentIdentification = agentIdentification;
        this.agentName = agentName;
        this.agentAccountNumber = agentAccountNumber;

    }

    /**
     * Constructotr that assists in getting headings for a CSV file.
     * These heasing are obtained from a loaded properties file.
     * @param properties
     */
    public AgentDetails( Properties properties){

        Validate.notNull( properties, "Error, When Reading Configured Headings. (Properties)");

        bankAccountNo = properties.getProperty("agent.detail.bank.account.number");
        transactionDescription = properties.getProperty("agent.detail.transaction.description");
        userReferenceNo = properties.getProperty("agent.detail.user.ref.no");
        transactionAmount = properties.getProperty("agent.detail.transaction.amount");
        businessDate = properties.getProperty("agent.detail.business.date");
        agentID = properties.getProperty("agent.detail.id");
        agentIdentification = properties.getProperty("agent.detail.identification");
        agentName = properties.getProperty("agent.detail.name");
        agentAccountNumber = properties.getProperty("agent.detail.account.number");

    }

    //Getter & Setters
    public String getBankAccountNo() {
        return bankAccountNo;
    }

    public void setBankAccountNo(String bankAccountNo) {
        this.bankAccountNo = bankAccountNo;
    }

    public String getTransactionDescription() {
        return transactionDescription;
    }

    public void setTransactionDescription(String transactionDescription) {
        this.transactionDescription = transactionDescription;
    }

    public String getUserReferenceNo() {
        return userReferenceNo;
    }

    public void setUserReferenceNo(String userReferenceNo) {
        this.userReferenceNo = userReferenceNo;
    }

    public String getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(String transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getBusinessDate() {
        return businessDate;
    }

    public void setBusinessDate(String businessDate) {
        this.businessDate = businessDate;
    }

    public String getAgentID() {
        return agentID;
    }

    public void setAgentID(String agentID) {
        this.agentID = agentID;
    }

    public String getAgentIdentification() {
        return agentIdentification;
    }

    public void setAgentIdentification(String agentIdentification) {
        this.agentIdentification = agentIdentification;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getAgentAccountNumber() {
        return agentAccountNumber;
    }

    public void setAgentAccountNumber(String agentAccountNumber) {
        this.agentAccountNumber = agentAccountNumber;
    }

}
