package za.co.javajoe.model;

/**
 * Created with IntelliJ IDEA.
 * User: letladi
 * Date: 2014/06/30
 * Time: 3:48 PM
 * To change this template use File | Settings | File Templates.
 */
public class Person {

    private int id;
    private String name;
    private String surname;
    private int age;
    private String race;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }


}
