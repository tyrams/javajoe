package za.co.javajoe.model;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: letladi
 * Date: 2014/06/27
 * Time: 12:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class EmployeeDetails implements Serializable {



    private String name;
    private double monthlySalary;
    private int age;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getMonthlySalary() {
        return monthlySalary;
    }

    public void setMonthlySalary(double monthlySalary) {
        this.monthlySalary = monthlySalary;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }




}
