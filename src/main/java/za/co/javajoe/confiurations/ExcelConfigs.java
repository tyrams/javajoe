package za.co.javajoe.confiurations;

import org.apache.commons.lang3.Validate;

import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: letladi
 * Date: 2014/08/11
 * Time: 10:40 AM
 * To change this template use File | Settings | File Templates.
 */
public class ExcelConfigs {

    private String filePath;

    public ExcelConfigs( Properties configProperties) {

        Validate.notNull(configProperties, "Error (NULL) when loading Configurations Properties");
        filePath =configProperties.getProperty("filePath");

    }

    public String getFilePath(){
        return filePath;
    }

}
