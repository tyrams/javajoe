package za.co.javajoe.confiurations;

import org.apache.commons.lang3.Validate;

import java.math.BigDecimal;
import java.util.Properties;

/**
 * AUTHOR: Thabo Matjuda
 * DATE: 08 August 2014
 * DESCRIPTION: This will just carry all the configurations from the configs.properties file.
 * So I will tryj to make sure that the configs.properties are loaded into this classe's properties.
 */
public class AmountConfigs {


    //Class Properties, Variables, Fields
    private BigDecimal minimumAmount;
    private BigDecimal maximumAmount;

    /**
     * No-Arg Constructor
     */
    public AmountConfigs() {}

    /**
     * Constructor that takes the Properties Object as Arg.
     * @param configProperties
     */
    public AmountConfigs( Properties configProperties) {

        Validate.notNull( configProperties, "Error (NULL) when loading Configurations Properties");

        this.minimumAmount = new BigDecimal( configProperties.getProperty("transaction.min.amount"));
        this.maximumAmount = new BigDecimal( configProperties.getProperty("transaction.max.amount"));


    }

    public BigDecimal getMinimumAmount() {
        return minimumAmount;
    }

    public BigDecimal getMaximumAmount() {
        return maximumAmount;
    }



}
