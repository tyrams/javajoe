package za.co.javajoe.services;

import za.co.javajoe.model.EmployeeDetails;
import org.apache.commons.lang3.Validate;

/**
 * Created with IntelliJ IDEA.
 * User: letladi
 * Date: 2014/06/27
 * Time: 2:09 PM
 * To change this template use File | Settings | File Templates.
 */
public class EmpBusinessLogic {

    /***
     * Calculates the total salary per annum
     * @param employeeDetails
     * @return
     */
    public double calaculateYearlySalary( EmployeeDetails  employeeDetails) {

        Validate.notNull( employeeDetails, "Come On Bro, Your Object (employeeDetails) Is NULL");

        double yearlySalary = 0;
        yearlySalary = employeeDetails.getMonthlySalary() * 12;
        return yearlySalary;

    }

    /**
     * Calculates yan employee's yearly increase based on certain conditions.
     * For example if your pay is  less than 10 000 your increase is 500
     * Otherwise the worker gets 1 000 increse.
     * @param employeeDetails
     * @return
     */
    public double calculateAppraisal( EmployeeDetails employeeDetails) {

        Validate.notNull( employeeDetails, "We Don't Workout Increases With (employeeDetails) That Are Null, Dude!");

        double appraisal = 0;

        if ( employeeDetails.getMonthlySalary() < 10000) {
            appraisal = 500;
        } else {
            appraisal = 1000;
        }

        return appraisal;
    }
}
