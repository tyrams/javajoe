package za.co.javajoe.services;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import za.co.javajoe.domain.AgentDetails;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @AUTHOR: Thabo Matjuda
 * @DATE: 19 August 2014
 * @DESCRIPTION: Various Service Operations for the AgentDetails Domain Object.
 */
public class AgentDetailsService {


    /**
     * To temporarily store Unique Details Of The Agents
     * @param agentDetailsList
     * @return
     */
    public Map<String, String> getUniqueAgentDetails( List<AgentDetails> agentDetailsList) {

        //Validation before we work with the object
        Validate.notNull( agentDetailsList, "Supplied List Object Is NULL");
        Validate.isTrue( agentDetailsList.size() > 0, "Supplied List Object SIZE Is Smaller Than Zero");

        for(AgentDetails item:agentDetailsList){

            System.out.println("===>******"+item.getAgentIdentification()+"--"+item.getAgentName()+"-*****-"+ item.getAgentAccountNumber());
            //System.out.println("===>" + item.getBankAccountNo()+"-"+item.getTransactionDescription()+"-");

        }

        //Method variables
        Map<String, String> agentDetailsMap = new HashMap<String, String>();

        //Looping through our agent details list
        for ( AgentDetails agentDetailItem : agentDetailsList) {

            /**
             * Check the map since it is not empty.
             * Basically checking if the Key (AgentID) is already in the list
             */
            if ( agentDetailsMap.size() > 0) {

                //What happens if the Key (Agent ID) is already in the Map??
                if ( agentDetailsMap.containsKey(  agentDetailItem.getAgentIdentification())) {
                    continue; //Just continue to next loop since we are done with this key
                }
            }

            /**
             * Ultimately at this stage just add in then.
             * At this stage it means that the key is not in the Map yet.
             */
            agentDetailsMap.put(agentDetailItem.getAgentIdentification(),
                    agentDetailItem.getAgentName() +" "+ agentDetailItem.getAgentAccountNumber());

        }

        for ( Map.Entry<String, String> entry : agentDetailsMap.entrySet()){

            String key = entry.getKey();
            String value = entry.getValue();

            System.out.println("Here are the values:"+key+"-"+value);

        }

        return agentDetailsMap;
    }

    /**
     * The point of this method is to replace the original agentID
     * To replace it with the actual details of the Agent, Agent Name & Agent Acc Number.
     * @param agentDetailsList
     * @param agentDetailsMap
     * @return
     */
    public List<AgentDetails> replaceAgentIDWithDetails( List<AgentDetails> agentDetailsList,
                                                         Map<String, String> agentDetailsMap) {


        //Validation Beofre working with the objects
        Validate.notNull( agentDetailsList, "Cannot Replace Info Inside NULL List (agentDetailsList)");
        Validate.isTrue(agentDetailsList.size() > 0, "Cannot Replace Info Inside List (agentDetailsList) With Size ZERO");
        Validate.notNull(agentDetailsMap, "Cannot Replace Info Of A NULL Map (agentDetailsMap)");
        Validate.isTrue(agentDetailsMap.size() > 0, "Cannot Replace Info Of A Map (agentDetailsMap) With Size ZERO");

        //Method variables section
        List<AgentDetails> ultimateAgentsList = new ArrayList<AgentDetails>();

        //Looping through the Map to get the keys and values
        for ( Map.Entry<String, String> mapRow : agentDetailsMap.entrySet()) {

            for ( AgentDetails agentDetailItem : agentDetailsList) {

                /**
                 * When the agentID is equal to the Map's Key
                 * Then replace the agentID with the Key's actual Value
                 */
                if ((agentDetailItem.getAgentID() != null ||
                        StringUtils.isNotEmpty( agentDetailItem.getAgentID()) ||
                        StringUtils.isNotBlank( agentDetailItem.getAgentID())) &&
                    agentDetailItem.getAgentID().compareTo( mapRow.getKey().toString()) == 0) {

                    agentDetailItem.setAgentID( mapRow.getValue());
                    ultimateAgentsList.add( agentDetailItem);

                }

            }

        }

        return ultimateAgentsList;

    }

}
