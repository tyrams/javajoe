package za.co.javajoe.services;

import za.co.javajoe.validation.AgentFileValidator;

/**
 * @AUTHOR: Thabo Matjuda
 * @DATE: 15 August 2014
 * @DESCRIPTION: Just a central class that gets the relevant implementations of the JoeValidator Interface.
 */
public final class JoeValidatorServiceFactory {

    /**
     * Gets the AgentFileValidator Implementation.
     * @return
     */
    public static AgentFileValidator getAgentFileValidator() {
        return new AgentFileValidator();
    }

}
