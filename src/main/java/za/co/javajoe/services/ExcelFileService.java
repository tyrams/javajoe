package za.co.javajoe.services;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * AUTHOR: Thabo Matjuda
 * DATE: 06 October 2014
 * DESCRIPTION:
 */
public class ExcelFileService {


    private String nullErrorMessage = "Object (%s) Is NULL";


    /**
     * Gets the Excel File's InputStream from the supplied filepath
     * @param filePath
     * @return
     * @throws FileNotFoundException
     */
    public FileInputStream getExcelFileInputStream( String filePath) throws FileNotFoundException {

        Validate.notNull( filePath, String.format( nullErrorMessage, "filePath"));

        if ( StringUtils.isEmpty( filePath) || StringUtils.isBlank( filePath)) {
            throw new RuntimeException("File path (filePath) is EMPTY / BLANK");
        }

        return new FileInputStream( filePath);
    }

    /**
     * Gets the The Excel's Workbook.
     * This is now obtained from the FileInput
     * @param fileInputStream
     * @return
     * @throws IOException
     */
    public XSSFWorkbook getExcelWorkBookFromInputStream( FileInputStream fileInputStream) throws IOException {
        Validate.notNull( fileInputStream, String.format( nullErrorMessage, "fileInputStream"));
        return new XSSFWorkbook( fileInputStream);

    }

    /**
     * Now using the Excel's Workbook we now get a sheet.
     * This sheet that we want is now obtained by name as well.
     * I mean we all know that an Excel Workbook / File can have many named sheets.
     * @param sheetName
     * @param xssfWorkbook
     * @return
     */
    public XSSFSheet getExcelSheetFromWorkbook( String sheetName, XSSFWorkbook xssfWorkbook) {
        Validate.notNull( xssfWorkbook, String.format( nullErrorMessage, "xssfWorkbook"));
        Validate.notNull( sheetName, String.format( nullErrorMessage, "sheetName"));

        if ( StringUtils.isEmpty( sheetName) || StringUtils.isBlank( sheetName)) {
            throw new RuntimeException("Sheet Name Arg. (sheetName) is EMPTY / BLANK");
        }

        return xssfWorkbook.getSheet( sheetName);
    }

    /**
     * Getting the Cell's Data as String.
     * This will convert most of the Datatypes we get to String.
     * @param xssfCell
     * @return
     */
    public String getCellDataAsString( XSSFCell xssfCell) {

        Validate.notNull( xssfCell, String.format( nullErrorMessage, "xssfCell"));

        switch ( xssfCell.getCellType()) {

            case Cell.CELL_TYPE_NUMERIC:
                return String.valueOf( xssfCell.getNumericCellValue());
            case Cell.CELL_TYPE_STRING:
                return xssfCell.getStringCellValue();
            case Cell.CELL_TYPE_BLANK:
                return "";

        }

        return "";
    }

}
