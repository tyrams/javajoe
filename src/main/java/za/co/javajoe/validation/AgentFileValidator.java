package za.co.javajoe.validation;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import za.co.javajoe.domain.AgentDetails;
import za.co.javajoe.keys.AgentDetailsFileKeys;

import java.util.List;
import java.util.Map;

/**
 * @AUTHOR: Thabo Matjuda
 * @DATE: 15 August 2014
 * @DESCRIPTION: Custom Home Made Validator for new AgentDetails File Process.
 *      This is used before processing the new data for the new CSV file.
 */
public class AgentFileValidator implements JoeValidator {

    private Map<AgentDetailsFileKeys, Object> argumentsMap;
    private List<AgentDetails> agentDetailsList;
    private String filePath;
    private String fileName;


    @Override
    public void validateStuff( Object objectToValidate) {

        Validate.notNull( objectToValidate, "Object To Validate Is NULL");

        //Main argument must be of Map type
        if ( !( objectToValidate instanceof Map)) {
            throw new RuntimeException("Expected Object (For Validation) To Be Type: "+ Map.class.getSimpleName());
        }

        argumentsMap = ( Map<AgentDetailsFileKeys, Object>) objectToValidate;

        //There must be Object of list type loaded in the map for thies Key
        if ( !( argumentsMap.get( AgentDetailsFileKeys.AGENT_DETAILS_LIST) instanceof List)) {
            throw new RuntimeException( AgentDetailsFileKeys.AGENT_DETAILS_LIST.toString()
                    + " Should Be Of Type "+ List.class.getSimpleName());
        }

        //This key must be for a Sring Object
        if ( !( argumentsMap.get( AgentDetailsFileKeys.AGENT_RESULT_FILE_PATH) instanceof String)) {
            throw new RuntimeException( AgentDetailsFileKeys.AGENT_RESULT_FILE_PATH.toString()
                    + " Should Be Of Type "+ String.class.getSimpleName());
        }

        //This key must be for a Sring Object
        if ( !( argumentsMap.get( AgentDetailsFileKeys.AGENT_DETAILS_FILE_NAME) instanceof String)) {
            throw new RuntimeException( AgentDetailsFileKeys.AGENT_DETAILS_FILE_NAME.toString()
                    + " Should Be Of Type "+ String.class.getSimpleName());
        }

        //Validation for the list Object (AgentDetails List)
        agentDetailsList = (List<AgentDetails>) argumentsMap.get( AgentDetailsFileKeys.AGENT_DETAILS_LIST);
        Validate.notNull( agentDetailsList, "AgentDetails List Is NULL");
        Validate.isTrue( agentDetailsList.size() > 0, "AgetnDetails List Size Is Not More Than ZERO. (NULL)");

        //Validation for the File Path
        filePath = (String) argumentsMap.get( AgentDetailsFileKeys.AGENT_RESULT_FILE_PATH);
        Validate.notNull( filePath, "File Path Is NULL");
        Validate.isTrue( filePath.length() > 0, "File Path Is NULL");

        if ( StringUtils.isEmpty( filePath) || StringUtils.isBlank( filePath)) {
            throw new RuntimeException("File Path Seems NULL (EMPTY), Dude");
        }

        //Validation for the given file name
        fileName = (String) argumentsMap.get( AgentDetailsFileKeys.AGENT_DETAILS_FILE_NAME);
        Validate.notNull( fileName, "File Name Is NULL");
        Validate.isTrue( fileName.length() > 0, "File Name Is NULL");

        if ( StringUtils.isEmpty( filePath) || StringUtils.isBlank( filePath)) {
            throw new RuntimeException("File Name Is Soooooo NULL (EMPTY), Sir");
        }

    }
}
