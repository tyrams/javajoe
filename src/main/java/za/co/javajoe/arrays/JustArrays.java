package za.co.javajoe.arrays;

import java.util.Arrays;
import java.util.List;

/**
 * AUTHOR: Thabo Matjuda
 * DATE: 29 June 2014
 * DESCRIPTION: A random class dealing with arrays in Java.
 * So this is nothing big, just playing with various things that you can do to arrays.
 * In short this is a Demonstration of Array Manipulation.
 */
public class JustArrays {

    private String[] peopleNames = null;
    private String[][] peopleFullNames = null;


    /***
     * Using Constructor to load the names into the array.
     */
    public JustArrays() {
        intializeNamesIntoArray();
    }

    /**
     * Gets array content or value by index and returns it back to caller.
     * @param nameIndex
     * @return
     */
    public String getPersonNameByIndex( int nameIndex) {
        return peopleNames[nameIndex];
    }

    /**
     * Just loads names (String) into the array that's all.
     */
    private void intializeNamesIntoArray() {

        peopleNames = new String[3];
        peopleNames[0] = "Mack";
        peopleNames[1] = "Tlou";
        peopleNames[2] = "Jerry";

        peopleFullNames = new String[5][8];

    }

    /**
     * Returns the instantiated and loaded array back to the caller/
     * @return
     */
    public String[] getPeopleNames() {
        return peopleNames;
    }

    /**
     * Displaying how to loop through arrays
     */
    public void printArrayContent() {

        for ( String nameItem : peopleNames) {
           System.out.println(nameItem);
        }

    }

    /**
     * Demosnstration on how to convert an ARRAY to a LIST.
     * Just for interest sake. Will Demo working with LISTS in Time.
     * @return
     */
    public List<String> convertArrayToList() {

       return Arrays.asList( peopleNames);

    }





}
