package za.co.javajoe.arrays;

import za.co.javajoe.model.Person;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: letladi
 * Date: 2014/06/30k
 * Time: 4:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class JustArraysExtended extends JustArrays {

    public static List<Person> personList = new ArrayList<Person>();
    int idCOunter = 0;

    public JustArraysExtended() {
        intializeNamesIntoList();
    }

    private void intializeNamesIntoList() {
        Person temp = new Person();

        temp.setId(1);
        temp.setAge(50);
        temp.setName("mackie");
        temp.setSurname("makgatho");
        temp.setRace("darkie");
        personList.add(temp);

        System.out.println(personList.size());


    }

    public Person getPersonObjectNameByIndex(int nameIndex) {
        for (Person personTemp : personList) {
            if (personTemp.getId() == nameIndex) {
                return personTemp;
            }
        }
        return null;
    }

    public List<Person> getPeopleObjectNames() {
        return personList;
    }

    public void printListContent() {

        for (Person nameItem : personList) {
            System.out.println(nameItem.getId() + " " + nameItem.getName());
        }

    }


}
