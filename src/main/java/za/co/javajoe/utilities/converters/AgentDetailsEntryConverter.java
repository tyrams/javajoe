package za.co.javajoe.utilities.converters;

import com.googlecode.jcsv.writer.CSVEntryConverter;
import za.co.javajoe.domain.AgentDetails;

/**
 * @AUTHOR: Thabo Matjuda
 * @DATE: 15 August 2014
 * @DESCRIPTION: This class is a special implementation.
 *          This implements Google's JCSV Framework / Tool.
 *          This entry converter for CSV files.
 *          It maps the columns to the AgentDetails Bean Properties and applies the real time data.
 *          This happens before writing the data to the CSV file.
 */
public class AgentDetailsEntryConverter implements CSVEntryConverter<AgentDetails> {

    /**
     * Implementation
     * @param agentDetails
     * @return
     */
    @Override
    public String[] convertEntry( AgentDetails agentDetails) {

        String[] fileColumns = new String[9];

        fileColumns[0] = agentDetails.getBankAccountNo();
        fileColumns[1] = agentDetails.getTransactionDescription();
        fileColumns[2] = agentDetails.getUserReferenceNo();
        fileColumns[3] = agentDetails.getTransactionAmount();
        fileColumns[4] = agentDetails.getBusinessDate();
        fileColumns[5] = agentDetails.getAgentID();
        fileColumns[6] = agentDetails.getAgentIdentification();
        fileColumns[7] = agentDetails.getAgentName();
        fileColumns[8] = agentDetails.getAgentAccountNumber();

        return fileColumns;
    }
}
