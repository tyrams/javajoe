package za.co.javajoe.utilities;

import org.apache.commons.lang3.Validate;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: letladi
 * Date: 2014/08/08
 * Time: 12:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class MinMaxUtil {


    public String readFromfile() {

        Properties properties = new Properties();
        InputStream input = null;
        String min_Prop="";
        String max_Prop="";

        try {

            String filename = "configs.properties";
            input = MinMaxUtil.class.getClassLoader().getResourceAsStream(filename);

            Validate.notNull( input, "Sorry Dud we cannot find the file");

            properties.load( input);

            System.out.println(properties.getProperty("transaction.min.amount"));
            System.out.println(properties.getProperty("transaction.max.amount"));


            min_Prop= properties.getProperty("transaction.min.amount");
            max_Prop= properties.getProperty("transaction.max.amount");

        } catch ( IOException ex) {
            ex.printStackTrace();
        } finally {
               if (input != null) {
                try {
                    input.close();
                } catch ( IOException e) {
                    e.printStackTrace();

                }
            }
        }

        return min_Prop+"|"+max_Prop;

    }





   public boolean checkIsWithinRange( BigDecimal tranAmount){

       //Check min condition
       String minMaxAmounts = readFromfile();
       String[] minMaxArray = minMaxAmounts.split("\\|", -1);
       String min = minMaxArray[0];
       String max = minMaxArray[1];

       //Compare:if value 0 then tranAmount equal min
       //Compare:if value 1 then tranAmount greater than min
       //Compare:if value -1 then tranAmount less than min
       if (( tranAmount.compareTo( new BigDecimal( min)) == 0 ||
               tranAmount.compareTo( new BigDecimal( min)) == 1) &&
               ( tranAmount.compareTo( new BigDecimal( max)) == 0 ||
                       tranAmount.compareTo( new BigDecimal(max)) == -1 ))  {

           System.out.println("OOOOH Dud your values are within Range");
           return true;

       }

       System.out.print("Dud you aint within Range");
       return false;
   }



}
