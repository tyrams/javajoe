package za.co.javajoe.utilities;

import com.googlecode.jcsv.CSVStrategy;
import com.googlecode.jcsv.writer.CSVWriter;
import com.googlecode.jcsv.writer.internal.CSVWriterBuilder;
import org.apache.commons.lang3.Validate;
import za.co.javajoe.domain.AgentDetails;
import za.co.javajoe.keys.AgentDetailsFileKeys;
import za.co.javajoe.services.JoeValidatorServiceFactory;
import za.co.javajoe.utilities.converters.AgentDetailsEntryConverter;
import za.co.javajoe.validation.JoeValidator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * @AUTHOR: Thabo Matjuda
 * @DATE: 15 August 2014
 * @DESCRIPTION: Various file utility methods / behaviors
 */
public final class FileUtil {

    private static JoeValidator validator;
    private static String agentDetailsConfigFileName = "agent-details.properties";



    /**
     * Produces a file.
     * This file is specific to the Agents Details Issue tackled by Mack @WIZZIT - International.
     * When he is ready, all he has to do is call this method to get the final file created.
     * @param agentFileArgMap
     * @return
     */
    public static File produceAgentDetialsCSVFile( Map<AgentDetailsFileKeys, Object> agentFileArgMap) {

        /**
         * Calling one of my home made validators to validate everything for me.
         * This is all before processing.
         */
        validator = JoeValidatorServiceFactory.getAgentFileValidator();
        Validate.notNull( validator, "Internal Application Error, Could Not Instantiate Validator For This Process");
        validator.validateStuff( agentFileArgMap);

        /**
         * Method variables on a more prominent level
         */
        Writer outFile = null;
        File actualFileObject = null;
        String actualFileName = null;
        String path = null;
        String fullFilePathAndName = null;
        List<AgentDetails> agentDetailsList = new ArrayList<AgentDetails>();

        try {

            //Collecting and fixing file path information.
            actualFileName = (String) agentFileArgMap.get( AgentDetailsFileKeys.AGENT_DETAILS_FILE_NAME);
            path = (String) agentFileArgMap.get( AgentDetailsFileKeys.AGENT_RESULT_FILE_PATH);
            fullFilePathAndName = path + actualFileName;

            //Creating fileObjects using the full file path.
            outFile = new FileWriter( fullFilePathAndName);
            actualFileObject = new File( fullFilePathAndName);

            /**
             * Calling my custom record entry converter implementation.
             * This maps to the actual CSV columns before writing the data out.
             */
            CSVWriter<AgentDetails> csvWriter = new CSVWriterBuilder<AgentDetails>( outFile).
                    strategy( CSVStrategy.UK_DEFAULT).
                    entryConverter( new AgentDetailsEntryConverter()).build();

            //Preparing heading & also then repacking everything underneath the heading
            agentDetailsList.add( getAgentDetailsFileHeadings());
            agentDetailsList.addAll( ( List<AgentDetails>) agentFileArgMap.get(
                    AgentDetailsFileKeys.AGENT_DETAILS_LIST));

            //Write data to file
            csvWriter.writeAll( agentDetailsList);

            //Basically save and close
            csvWriter.flush();
            csvWriter.close();

            System.out.println("FILE SAVED TO: "+ fullFilePathAndName);

        } catch ( IOException e) {
            e.printStackTrace();
            throw new RuntimeException( e.getMessage());
        } catch ( ClassCastException e) {
            e.printStackTrace();
            throw new RuntimeException( e.getMessage());
        }

        return actualFileObject;

    }

    /**
     * Just Static Information that will carry the headings for the file.
     * @return AgentDetails Object
     */
    public static AgentDetails getAgentDetailsFileHeadings() {
        return new AgentDetails( ConfigUtil.loadConfigurations( agentDetailsConfigFileName));
    }


    public static List<AgentDetails> processList( List<AgentDetails> agentDetailsList){

        return null;
    }



}
