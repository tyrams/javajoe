package za.co.javajoe.utilities;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayInputStream;
import java.io.StringWriter;

/**
 * AUTHOR: Thabo Matjuda
 * DATE: 20 October 2014
 * DESCRIPTION: For various XML & String Manipulation process.
 */
public class XMLUtil {


    private static String nullErrorMessage = "The Argument You Have Supplied (Object) Is NULL";

    //Class variables, these can be configurable.
    private static String ENCODING = "UTF-8";
    private static String OMIT_XML_DECLARATION = "yes";
    private static String SHOULD_INDENT = "yes";
    private static String INDENTATION_AMOUNT = "4";
    private static String OUTPUT_PROPERTY_NAME = "{http://xml.apache.org/xslt}indent-amount";



    /**
     * Formats the supplied String object to nicer / proper XML format.
     * @param stringXML: The normal simple XML you want to format into proper XML
     * @return String object of formatted XML.
     */
    public static String toXMLFormat( String stringXML) {

        Validate.notNull( stringXML, nullErrorMessage);

        if ( StringUtils.isEmpty( stringXML) || StringUtils.isBlank( stringXML)) {
            throw new RuntimeException("Your Argument Is EMPTY / BLANK");
        }

        try {

            Document document = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder()
                    .parse( new InputSource( new ByteArrayInputStream( stringXML.getBytes( ENCODING))));

            XPath xPath = XPathFactory.newInstance().newXPath();
            NodeList nodeList = (NodeList) xPath.evaluate("//text()[normalize-space()='']",
                    document,
                    XPathConstants.NODESET);

            for (int i = 0; i < nodeList.getLength(); ++i) {
                Node node = nodeList.item(i);
                node.getParentNode().removeChild(node);
            }

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty( OutputKeys.ENCODING, ENCODING);
            transformer.setOutputProperty( OutputKeys.OMIT_XML_DECLARATION, OMIT_XML_DECLARATION);
            transformer.setOutputProperty( OutputKeys.INDENT, SHOULD_INDENT);
            transformer.setOutputProperty( OUTPUT_PROPERTY_NAME, INDENTATION_AMOUNT);

            StringWriter stringWriter = new StringWriter();
            StreamResult streamResult = new StreamResult( stringWriter);

            transformer.transform( new DOMSource( document), streamResult);

            return stringWriter.toString();

        } catch (Exception e) {
            throw new RuntimeException( e.getMessage());
        }
    }

}
