package za.co.javajoe.utilities;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import za.co.javajoe.confiurations.AmountConfigs;
import za.co.javajoe.confiurations.ExcelConfigs;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * AUTHOR: Thabo Matjuda
 * DATE: 08 August 2014
 * DESCRIPTION: Class just for utilities.
 * This will deal with more configurations for now. Anything regarding configurations should be done in here.
 * For example initializing / bootstrapping / loading them.
 */
public final class ConfigUtil {


    private static AmountConfigs amountConfigs = null;
    private static ExcelConfigs  excelConfigs = null;



    /**
     * Returns the preloaded Object.
     * Preloaded with the configurations.
     * @return JoeConfigs Object
     */
    public static void loadConfigurations() {

        Properties prop = new Properties();
        InputStream input = null;

        try {

            String fileName = "configs.properties";

            //Reading the properties file through an Input Stream
            input = ConfigUtil.class.getClassLoader().getResourceAsStream( fileName);

            Validate.notNull("Sorry, Dude, Unable To Find File, " + fileName);

            //Load a properties file from class path, inside static method
            prop.load(input);

            //Assigning a preloaded Config Object
            amountConfigs = new AmountConfigs( prop);

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally{
            if(input!=null){
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    /**
     * Returns the preloaded AmountConfigs Object
     * @return AmountConfigs Object
     */
    public static AmountConfigs getAmountConfigs() {
        loadConfigurations();
        Validate.notNull( amountConfigs, "Error, while loading amount configs");
        return amountConfigs;
    }

    /**
     * Returns the preloaded Object.
     * Preloaded with the configurations.
     * @param fileName
     * @return Properties Object loaded with the configurations.
     */
    public static Properties loadConfigurations( String fileName) {

        String errorMessage = "File Name Is NULL (EMPTY)";

        //Performing some validation
        Validate.notNull( fileName, errorMessage);

        if ( StringUtils.isEmpty( fileName) || StringUtils.isBlank( fileName)) {
           throw new RuntimeException( errorMessage);
        }

        Properties prop = new Properties();
        InputStream input = null;

        try {

            //Reading the properties file through an Input Stream
            input = ConfigUtil.class.getClassLoader().getResourceAsStream( fileName);

            //Load a properties file from class path, inside static method
            prop.load(input);

            return prop;

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally{
            if(input!=null){
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return null;

    }

    /**
     * Loads an excel sheet.
     * So far this method loads an excel sheet by name.
     * @return HSSFSheet Object
     */
    public static HSSFSheet loadExcelSheet() {

        HSSFSheet  worksheet = null;

        String error = "Sorry Sir Excel file is not found";
        excelConfigs =  new ExcelConfigs( loadConfigurations("configs.properties"));
        String path  = excelConfigs.getFilePath();
        Validate.notNull( path, "Path config not found");

        try {

            FileInputStream fileInputStream = new FileInputStream( path);
            Validate.notNull( fileInputStream, error);

            HSSFWorkbook workbook = new HSSFWorkbook( fileInputStream);
            worksheet = workbook.getSheet("Sheet1");

           /* HSSFRow row1 = worksheet.getRow(1);
            HSSFCell cellA1 = row1.getCell((short) 0);
            String a1Val = cellA1.getStringCellValue();
            System.out.println("===>" + a1Val);
*/
            return worksheet;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;

    }

    /**
     * Justy also remember to comment here, general
     * @return
     */
    public static String testConfig() {
        return "Nelson's first ppush";
    }
}
