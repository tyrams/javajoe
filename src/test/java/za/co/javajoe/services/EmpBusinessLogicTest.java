package za.co.javajoe.services;

import za.co.javajoe.model.EmployeeDetails;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.*;

import static org.hamcrest.MatcherAssert.assertThat;

public class EmpBusinessLogicTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    EmployeeDetails employeeDetails = null;
    EmpBusinessLogic empBusinessLogic = null;

    @Before
    public void setUp() throws Exception {

        empBusinessLogic = new EmpBusinessLogic();

        employeeDetails = new EmployeeDetails();
        employeeDetails.setAge( 27);
        employeeDetails.setMonthlySalary( 12000);
        employeeDetails.setName("Mackito");

    }

    /**
     * Demoing how to test the validation we added.
     * Baically in the business logic we validate that before we calculate the salary, we are not working with a NUll worker.
     * If so then our validation should kick in.
     * This is the fist #1 way you can show that you are expecting an Exception. Simpler.
     * @throws Exception
     */
    @Test(expected = NullPointerException.class)
    public void testCalaculateYearlySalaryNullPointerExpected() throws Exception {

        employeeDetails = null;
        empBusinessLogic.calaculateYearlySalary( employeeDetails);

    }

    /**
     * This other way is more complex for those that are used to using the way demonstrated above.
     * Demoing how to test the validation we added.
     * Baically in the business logic we validate that before we calculate the salary, we are not working with a NUll worker.
     * If so then our validation should kick in.
     * This is the fist #2 way you can show that you are expecting an Exception. Simpler.
     * @throws Exception
     */
    @Test
    public void testCalaculateYearlySalaryNullPointerExpectedComplex() throws Exception {

        employeeDetails = null;

        //Show that I expect the Exception before calling the logic that will throw it.
        expectedException.expect( NullPointerException.class);
        expectedException.expectMessage("Come On Bro, Your Object (employeeDetails) Is NULL");

        //Then do the call afterwards
        empBusinessLogic.calaculateYearlySalary( employeeDetails);

    }

    @Test
    public void testCalculateAppraisalNullPointerExpectedComplex() throws Exception {

        employeeDetails = null;

        //Show that I expect the Exception before calling the logic that will throw it.
        expectedException.expect( NullPointerException.class);
        expectedException.expectMessage("We Don't Workout Increases With (employeeDetails) That Are Null, Dude!");

        //Then do the call afterwards
        empBusinessLogic.calculateAppraisal( employeeDetails);

    }

    @Test
    public void testCalaculateYearlySalary() throws Exception {

        assertThat( empBusinessLogic.calaculateYearlySalary( employeeDetails), equalTo( 144000D));

    };

    @Test
    public void testCalculateAppraisal() throws Exception {

        assertThat( empBusinessLogic.calculateAppraisal( employeeDetails), equalTo( 1000D));

    }

    @Test
    public void testNewSalaryAfterAppraisal() throws Exception {

        assertThat( empBusinessLogic.calculateAppraisal( employeeDetails)
                + employeeDetails.getMonthlySalary(), equalTo( 13000D));

    }
}