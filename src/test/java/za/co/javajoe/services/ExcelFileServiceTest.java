package za.co.javajoe.services;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;

import java.io.FileInputStream;

import static org.junit.Assert.*;

public class ExcelFileServiceTest {


    @Test
    public void testGetExcelFileInputStream() throws Exception {

        String filePath = "C:\\spaces\\files\\excel-example.xlsx";

        ExcelFileService efService = new ExcelFileService();

        FileInputStream fileInputStream = efService.getExcelFileInputStream( filePath);
        assertNotNull( fileInputStream);

        XSSFWorkbook xssfWorkbook = efService.getExcelWorkBookFromInputStream( fileInputStream);
        assertNotNull( xssfWorkbook);

        XSSFSheet xssfSheet = efService.getExcelSheetFromWorkbook("sheet1", xssfWorkbook);
        assertNotNull( xssfSheet);

    }

    @Test
    public void testColumnNames() throws Exception {

        String filePath = "C:\\spaces\\files\\excel-example.xlsx";

        ExcelFileService efService = new ExcelFileService();

        FileInputStream fileInputStream = efService.getExcelFileInputStream( filePath);
        assertNotNull( fileInputStream);

        XSSFWorkbook xssfWorkbook = efService.getExcelWorkBookFromInputStream( fileInputStream);
        assertNotNull( xssfWorkbook);

        XSSFSheet xssfSheet = efService.getExcelSheetFromWorkbook("sheet1", xssfWorkbook);
        assertNotNull( xssfSheet);

    }
}