package za.co.javajoe.confiurations;

import org.junit.Test;
import za.co.javajoe.utilities.ConfigUtil;

import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertNotNull;

/**
 * Created with IntelliJ IDEA.
 * User: letladi
 * Date: 2014/08/11
 * Time: 12:24 PM
 * To change this template use File | Settings | File Templates.
 */
public class ExcelConfigsTest {


    @Test
    public void testGetConfigPath() throws Exception {

        ExcelConfigs excelConfigs = new ExcelConfigs(
                ConfigUtil.loadConfigurations("configs.properties"));

        assertNotNull( excelConfigs.getFilePath());
        assertTrue( excelConfigs.getFilePath().length() > 0);

    }
}
