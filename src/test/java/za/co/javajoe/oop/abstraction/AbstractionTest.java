package za.co.javajoe.oop.abstraction;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @Created by TLOU on 2014/07/01.
 *
 * @NOTES:  This test class basically shows how abstraction works,
 *          below are 4 test methods testing 2 StringedInstrument
 *          classes which extends the base class Instrument.
 *
 * @PleaseNote: Both Classes override the abstract method play()
 *              which is declared in the base class Instrument
 *
 *              Take a good look at the name of the test method
 *              to see what is being tested  from the classes.
 *
 */
public class AbstractionTest {

    String electric6String = "A 6 String Electric Guitar is Awesome!!!";
    String electric12String = "A 12 String Electric Guitar is Awesome!!!";

    String bass4String = "A 4 String Bass Guitar is Awesome!!!";
    String bass6String = "A 6 String Bass Guitar is Awesome!!!";

    @Test
    public void testElectricGuitarDefaultConstructedClass(){

        ElectricGuitar electricGuitar = new ElectricGuitar();
        assertThat( electricGuitar.play(), equalTo(electric6String));
        System.out.println(electric6String);
    }

    @Test
    public void testElectricOverloadedConstructedClass(){

        ElectricGuitar electricGuitar = new ElectricGuitar(12);
        assertThat(electricGuitar.play(), equalTo(electric12String));
        System.out.println(electric12String);
    }

    @Test
    public void testBassGuitarDefaultConstructedClass(){

        BassGuitar bassGuitar = new BassGuitar();
        assertThat( bassGuitar.play(), equalTo(bass4String));
        System.out.println(bass4String);
    }

    @Test
    public void testBassOverloadedConstructedClass(){

        BassGuitar bassGuitar = new BassGuitar(6);
        assertThat(bassGuitar.play(), equalTo(bass6String));
        System.out.println(bass6String);
    }
}
