package za.co.javajoe.oop.inheritance;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.Test;
import za.co.javajoe.oop.inheritance.checkForInheritance;
import za.co.javajoe.oop.inheritance.classA;
import za.co.javajoe.oop.inheritance.classB;
import za.co.javajoe.oop.inheritance.classC;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Created with IntelliJ IDEA.
 * User: letladi
 * Date: 2014/07/02
 * Time: 11:06 AM
 * To change this template use File | Settings | File Templates.
 */
public class inheritenceTest {

    @Test
    public void firstCheck(){

      checkForInheritance checker = new checkForInheritance();
      assertThat(checker.firstCheck(),equalTo("this is method in classB,firstVriableforA"));

    }
}
