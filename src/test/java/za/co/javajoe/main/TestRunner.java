package za.co.javajoe.main;


import org.junit.runner.*;
import org.junit.runner.notification.Failure;

/**
 * Created with IntelliJ IDEA.
 * User: letladi
 * Date: 2014/06/25
 * Time: 11:29 AM
 * To change this template use File | Settings | File Templates.
 */
public class TestRunner {
    public static void main(String[] args){
        Result result = JUnitCore.runClasses(HelloJavaJoeTest.class);
        for(Failure failure:result.getFailures()){
            System.out.println(failure.toString());
        }
      System.out.println(result.wasSuccessful());
    }
}
