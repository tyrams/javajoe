package za.co.javajoe.main;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class HelloJavaJoeTest {

    @Test
    public void testSayHello() throws Exception {

        HelloJavaJoe helloJavaJoe = new HelloJavaJoe();

        String name = "Tlou";
        String message = "Ola, " + name;

        assertThat( helloJavaJoe.sayHello(name), equalTo( message));


    }

    @Test(expected = NullPointerException.class)
    public void testSayHelloError() throws Exception {

        HelloJavaJoe helloJavaJoe = new HelloJavaJoe();

        String name = null;
        String message = "Ola, " + name;

        //using hamCrest libs to test
        assertThat( helloJavaJoe.sayHello(name), equalTo( name));


    }

    @Test
    public void testAddNumbers()throws Exception{

        HelloJavaJoe helloJavaJoe =new HelloJavaJoe();
        int numberArray[]={1,2,3,4,5,6};
        int total= 21;

        assertThat(helloJavaJoe.addNumbers(numberArray),equalTo(total));

    }


   /* @Test
    public void testPrintMessage(){
        String message="hello World";
        MessageUtil messageUtil = new MessageUtil(message);
        //assertThat(messageUtil.printMessage(),equalTo(message));
        assertEquals(message,messageUtil.printMessage());

    }*/
}