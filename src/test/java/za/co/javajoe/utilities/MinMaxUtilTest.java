package za.co.javajoe.utilities;

import org.junit.Test;

import java.math.BigDecimal;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: letladi
 * Date: 2014/08/08
 * Time: 2:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class MinMaxUtilTest {


    @Test
    public void testReadFromfile() throws Exception {

        MinMaxUtil minMaxUtil = new MinMaxUtil();

        assertNotNull( minMaxUtil.readFromfile());

    }

    @Test
    public void testCheckIsWithinRange() throws Exception {

        MinMaxUtil minMaxUtil = new MinMaxUtil();

        BigDecimal tranAmount = new BigDecimal("09.00");
        assertFalse( minMaxUtil.checkIsWithinRange( tranAmount));

        tranAmount = new BigDecimal("351.00");
        assertFalse( minMaxUtil.checkIsWithinRange( tranAmount));

        tranAmount = new BigDecimal("289.00");
        assertTrue(minMaxUtil.checkIsWithinRange( tranAmount));

    }
}
