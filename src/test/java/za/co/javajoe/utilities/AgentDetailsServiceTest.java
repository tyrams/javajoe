package za.co.javajoe.utilities;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import za.co.javajoe.domain.AgentDetails;
import za.co.javajoe.services.AgentDetailsService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

/**
 * Created with IntelliJ IDEA.
 * User: letladi
 * Date: 2014/08/19
 * Time: 9:58 AM
 * To change this template use File | Settings | File Templates.
 *
 */
public class AgentDetailsServiceTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testgetUniqueAgentDetails() throws Exception{

        AgentDetailsService agentDetailsService = new AgentDetailsService() ;
        Map<String, String> agentNewDetailsMap = new HashMap<String, String>();;
        HSSFSheet worksheet =  ConfigUtil.loadExcelSheet();
        assertNotNull( worksheet);
        List<AgentDetails> collection = JoeUtil.mapExacelToObject(worksheet);
        assertNotNull( collection);

        agentNewDetailsMap = agentDetailsService.getUniqueAgentDetails(collection);
        assertNotNull(agentNewDetailsMap);





    }

   @Test
    public void testreplaceAgentIDWithDetails() throws Exception{

        AgentDetailsService agentDetailsService = new AgentDetailsService() ;
        List<AgentDetails> ultimateList = new ArrayList<AgentDetails>();

        Map<String, String> agentNewDetailsMap = new HashMap<String, String>();;
        HSSFSheet worksheet =  ConfigUtil.loadExcelSheet();
        assertNotNull( worksheet);
        List<AgentDetails> collection = JoeUtil.mapExacelToObject(worksheet);
        assertNotNull( collection);

        agentNewDetailsMap = agentDetailsService.getUniqueAgentDetails(collection);
        assertNotNull(agentNewDetailsMap);

        ultimateList = agentDetailsService.replaceAgentIDWithDetails(collection,agentNewDetailsMap);
        assertNotNull(ultimateList);

       System.out.println("==============================================");

        for(AgentDetails item:ultimateList){

         System.out.println("New object:::"+item.getBankAccountNo() +":::"+item.getTransactionDescription() +":::"+item.getUserReferenceNo()+":::"+item.getTransactionAmount()+":::"+item.getBusinessDate()+":::---->"+item.getAgentID()+":::---->"+item.getAgentIdentification());
            //System.out.println("===>" + item.getBankAccountNo()+"-"+item.getTransactionDescription()+"-");

        }

    }

}
