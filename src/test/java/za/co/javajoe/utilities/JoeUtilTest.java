package za.co.javajoe.utilities;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import za.co.javajoe.arrays.JustArrays;
import za.co.javajoe.constants.StringType;
import za.co.javajoe.domain.Account;
import za.co.javajoe.domain.Customer;

import java.awt.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import static org.hamcrest.MatcherAssert.assertThat;

public class JoeUtilTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();



    @Test
    public void testConvertStringArrayToList() throws Exception {

        JustArrays justArrays =  new JustArrays();
        assertNotNull("Your (peopleNames) Array Is NULL, DUDE.", justArrays.getPeopleNames());

        List<String> peopleNamesList = JoeUtil.convertStringArrayToList( justArrays.getPeopleNames());
        assertNotNull("List Is Empty, Champ", peopleNamesList);

        for ( String listNames : peopleNamesList) {
           System.out.println("Name Is: "+ listNames);
        }

        System.out.println("Congratulations! Looks like you have converted array to list successfully!");

    }

    @Test
    public void testAmountIsGreaterThanZero() throws Exception {

        BigDecimal testAmount = null;
        assertFalse( JoeUtil.amountIsGreaterThanZero( testAmount));

        testAmount = BigDecimal.ZERO;
        assertFalse( JoeUtil.amountIsGreaterThanZero( testAmount));

        exception.expect( NumberFormatException.class);
        testAmount = new BigDecimal("");

        exception.expect( NumberFormatException.class);
        exception.expectMessage("java.lang.NumberFormatException");
        testAmount = new BigDecimal("asfdasd");

        testAmount = new BigDecimal("-1");
        assertFalse( JoeUtil.amountIsGreaterThanZero( testAmount));

        testAmount = new BigDecimal("0");
        assertFalse( JoeUtil.amountIsGreaterThanZero( testAmount));

        testAmount = new BigDecimal("1");
        assertTrue( JoeUtil.amountIsGreaterThanZero( testAmount));

    }

    @Test
    public void testCustomersAndAccounts() throws Exception {

        List<Customer> customerList = JoeUtil.getCustomers();
        List<Account> accountList = JoeUtil.getAccounts();

        assertNotNull( customerList);
        assertNotNull( accountList);
        assertTrue( customerList.size() > 0);
        assertTrue( accountList.size() > 0);

        Map<Long, String> tempMap = new HashMap<Long, String>();

        //Looping through customers
        for ( Customer customer : customerList) {

            /*
            * Here I am catching the scenario where the Map already has something.
            * We need to do a check to see if that current Loop Key is already inside the Map.
            * */
            if ( tempMap.size() > 0) {

                if ( tempMap.containsKey( Long.valueOf( customer.getAccountID()))) {

                    //Since the key (accountID) already exists then pull the existing value
                    String existingNames = tempMap.get( Long.valueOf( customer.getAccountID()));

                    //Append then new value (Customer Name) to the existing one since they share the same value (AccID).
                    existingNames = existingNames + customer.getName() + "|";

                    //When done with appeding, then push back the new value to the same key
                    tempMap.put( Long.valueOf( customer.getAccountID()), existingNames);

                    //Nothing more so we continue to the next Entry loop
                    continue;

                }

            }

            /*
            * Just add a new unique key.
            * Only if the key does not exist in the Map
            * */
            tempMap.put( Long.valueOf( customer.getAccountID()), customer.getName() + "|");

        }

        assertTrue( tempMap.size() > 0);

        /*
        * Demonstration of how to loop through a map
        * Just loop through the map and Print out stuff iside it (Keys & Values)
        * */
        for ( Map.Entry<Long, String> mapRow : tempMap.entrySet()) {

            System.out.println("ACCOUNT NUMBER: "+ JoeUtil.getAccountNumberFromID(
                    accountList, mapRow.getKey().toString()) + " IS USED BY: "+ mapRow.getValue());


        }

        //ACCOUNT NUMBER: 6222-12345 IS USED BY: Jeremiah|Thali|Gugu|Rudzani|
        //ACCOUNT NUMBER: 6333-12346 IS USED BY: Thabo|Aubrey|Rudzani|
        //ACCOUNT NUMBER: 6444-12347 IS USED BY: Mack|Nelson|Prince|

    }

    @Test
    public void testAmountIsWithinRange() throws Exception {

        BigDecimal tranAmount = new BigDecimal("05.00");
        assertFalse( JoeUtil.amountIsWithinRange( tranAmount));

        tranAmount = new BigDecimal("10.00");
        assertTrue( JoeUtil.amountIsWithinRange( tranAmount));

        tranAmount = new BigDecimal("351.00");
        assertFalse( JoeUtil.amountIsWithinRange( tranAmount));

        tranAmount = new BigDecimal("280.00");
        assertTrue(JoeUtil.amountIsWithinRange( tranAmount));

    }

    @Test
    public void testCalculateWordLetters() throws Exception {

        JoeUtil.calculateWordLetters("Matjuda");
        JoeUtil.calculateWordLetters("Gugu");
        JoeUtil.calculateWordLetters("Lebogang");

    }

    @Test
    public void testRemoveAmountBrackets() throws Exception {

        String amount = "374695";
        String changedAmount = "";

        changedAmount = JoeUtil.removeRoundBrackets( amount);
        assertThat( changedAmount, equalTo( amount));

        amount = "(55577)";

        changedAmount = JoeUtil.removeRoundBrackets( amount);
        assertThat( changedAmount, equalTo( "-55577"));

    }

    @Test
    public void testIsValidString() throws Exception {

        assertTrue( JoeUtil.isValidString("Stuff Here", StringType.Standard));
        assertTrue( JoeUtil.isValidString("Stuff Here", StringType.General));
        assertTrue( JoeUtil.isValidString("Stuff Here", StringType.Extended));
        assertFalse( JoeUtil.isValidString("Stuff Here", StringType.Numeric));

    }

    @Test
    public void testRemoveEOLSpace() throws Exception {

        String originalOne = "HENRY GROVER SILANDA                                ";
        System.out.println( originalOne);
        String comeBack = JoeUtil.removeEOLSpace( originalOne);
        System.out.println( comeBack);
        assertTrue( comeBack.equals("HENRY GROVER SILANDA"));

    }

    @Test
    public void testGetOrigSortCode() throws Exception {

        String sortCodeRes = JoeUtil.getOrigSortCode("ORIG:110804;0045100628007;1");
        System.out.println( sortCodeRes);
        assertThat( sortCodeRes, equalTo("110804"));

    }
}