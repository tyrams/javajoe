package za.co.javajoe.utilities;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.junit.Test;
import za.co.javajoe.domain.AgentDetails;
import za.co.javajoe.keys.AgentDetailsFileKeys;
import za.co.javajoe.services.AgentDetailsService;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class FileUtilTest {

    @Test
    public void testGetAgentDetailsFileHeadings() throws Exception {

        assertNotNull( FileUtil.getAgentDetailsFileHeadings());

        AgentDetails agentDetails = FileUtil.getAgentDetailsFileHeadings();

        assertNotNull( agentDetails.getBankAccountNo());
        assertTrue(agentDetails.getBankAccountNo().length() > 0);

        assertNotNull(agentDetails.getTransactionDescription());
        assertTrue(agentDetails.getTransactionDescription().length() > 0);

        assertNotNull(agentDetails.getUserReferenceNo());
        assertTrue(agentDetails.getUserReferenceNo().length() > 0);

        assertNotNull(agentDetails.getTransactionAmount());
        assertTrue(agentDetails.getTransactionAmount().length() > 0);

        assertNotNull(agentDetails.getBusinessDate());
        assertTrue(agentDetails.getBusinessDate().length() > 0);

        assertNotNull(agentDetails.getAgentID());
        assertTrue(agentDetails.getAgentID().length() > 0);

        assertNotNull(agentDetails.getAgentIdentification());
        assertTrue(agentDetails.getAgentIdentification().length() > 0);

        assertNotNull(agentDetails.getAgentName());
        assertTrue(agentDetails.getAgentName().length() > 0);

        assertNotNull(agentDetails.getAgentAccountNumber());
        assertTrue( agentDetails.getAgentAccountNumber().length() > 0);

    }

    @Test
    public void testProduceAgentDetialsCSVFile() throws Exception {

        Map<AgentDetailsFileKeys, Object> testArgumentMap = new HashMap<AgentDetailsFileKeys, Object>();
        List<AgentDetails> agentDetailsList = new ArrayList<AgentDetails>();

        assertNotNull("File Headings NULL", FileUtil.getAgentDetailsFileHeadings());
        agentDetailsList.add( FileUtil.getAgentDetailsFileHeadings());
        testArgumentMap.put( AgentDetailsFileKeys.AGENT_DETAILS_LIST, agentDetailsList);

        String filePath = ConfigUtil.loadConfigurations("configs.properties").getProperty("result.file.path.thabo");
        assertNotNull( filePath);
        assertTrue( filePath.length() > 0);
        testArgumentMap.put( AgentDetailsFileKeys.AGENT_RESULT_FILE_PATH, filePath);

        testArgumentMap.put( AgentDetailsFileKeys.AGENT_DETAILS_FILE_NAME, "test-file.csv");

        File file = FileUtil.produceAgentDetialsCSVFile( testArgumentMap);
        assertNotNull( file);

    }


    @Test
    public void  testUltimatelyProduceAgentDetialsCSVFile() throws Exception{

        AgentDetailsService agentDetailsService = new AgentDetailsService() ;
        List<AgentDetails> ultimateList = new ArrayList<AgentDetails>();

        Map<String, String> agentNewDetailsMap = new HashMap<String, String>();;

        HSSFSheet worksheet =  ConfigUtil.loadExcelSheet();
        assertNotNull( worksheet);
        List<AgentDetails> collection = JoeUtil.mapExacelToObject(worksheet);
        assertNotNull( collection);

        agentNewDetailsMap = agentDetailsService.getUniqueAgentDetails(collection);
        assertNotNull(agentNewDetailsMap);

        ultimateList = agentDetailsService.replaceAgentIDWithDetails(collection,agentNewDetailsMap);
        assertNotNull( ultimateList);

        Map<AgentDetailsFileKeys, Object> agentFileArgMap = new HashMap<AgentDetailsFileKeys, Object>();

        agentFileArgMap.put( AgentDetailsFileKeys.AGENT_DETAILS_LIST, ultimateList);
        agentFileArgMap.put( AgentDetailsFileKeys.AGENT_RESULT_FILE_PATH,
                ConfigUtil.loadConfigurations("configs.properties").getProperty("result.file.path.mack"));
        agentFileArgMap.put( AgentDetailsFileKeys.AGENT_DETAILS_FILE_NAME, "mack-test.csv");

        File file = FileUtil.produceAgentDetialsCSVFile( agentFileArgMap);
        assertNotNull( file);

        System.out.println( file.getCanonicalPath());
        System.out.println( file.getName());


    }
}