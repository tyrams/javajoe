package za.co.javajoe.utilities;

import org.junit.Test;

import static org.junit.Assert.*;

public class XMLUtilTest {

    @Test
    public void testToXMLFormat() throws Exception {

        String stringXML = "<env:Envelope xmlns:env='http://schemas.xmlsoap.org/soap/envelope/'>" +
                "<env:Header></env:Header><env:Body>" +
                "<CREATECONTRACT_FSFS_REQ xmlns='http://fcubs.ofss.com/service/FCUBSFTService'>" +
                "<FCUBS_HEADER><SOURCE>SFISWITCH</SOURCE><UBSCOMP>FCUBS</UBSCOMP><CORRELID>3AF8770EA562</CORRELID>" +
                "<USERID>SYSTEM</USERID><BRANCH>016</BRANCH><MODULEID>FT</MODULEID><SERVICE>FCUBSFTService</SERVICE" +
                "><OPERATION>CreateContract</OPERATION><ACTION>NEW</ACTION><MSGSTAT>SUCCESS</MSGSTAT></FCUBS_HEADER>" +
                "<FCUBS_BODY><Contract-Details-Full><CONTREFNO></CONTREFNO><PROD>RSFI</PROD>" +
                "<USERREFNO>3AF8770EA562</USERREFNO><DRVALDT>2014-10-16</DRVALDT><CRBRN>018</CRBRN>" +
                "<CRACC>3000008000126</CRACC><CRCCY>ZMW</CRCCY><CRAMT>1105.0200</CRAMT><CRVALDT>2014-10-16</CRVALDT>" +
                "<DBTBRN>003</DBTBRN><DBTACC>1219234000280</DBTACC><DRCCY>ZMW</DRCCY><DR_AMOUNT>1105.0200</DR_AMOUNT>" +
                "<CHRGBEARER>O</CHRGBEARER><SOURCEREFNO>3AF8770EA562</SOURCEREFNO><SOURCECD>SFISWITCH</SOURCECD>" +
                "<PAYMENTDET1></PAYMENTDET1><BYORDEROF1>1219234000280</BYORDEROF1>" +
                "<ULTBEN1>/9999999</ULTBEN1><ULTBEN2>CHEKANI T SAKALA 192 OCT 2011</ULTBEN2>" +
                "<BOOKDT>2014-10-16</BOOKDT><USRREF>3AF8770EA562</USRREF><BRN>016</BRN><PRDCD>RSFI</PRDCD>" +
                "<Settlements><Contract-Settlement><COMPNT>TFR_AMT</COMPNT><TAGCCY>ZMW</TAGCCY><ACCBRN>018</ACCBRN>" +
                "<ACC>3000008000126</ACC><ACCCCY>ZMW</ACCCCY><GENMSG>Y</GENMSG><TRANSTYP>C</TRANSTYP><PAYMNTDET1>" +
                "</PAYMNTDET1><SNDTRRCVINFO1>/0604/</SNDTRRCVINFO1><SNDTRRCVINFO2>/1030/</SNDTRRCVINFO2>" +
                "<COVREQRD>N</COVREQRD><ORDCUST1>/1219234000280</ORDCUST1><ORDCUST2>Grz May Account</ORDCUST2>" +
                "<ULTBEN1>/9999999</ULTBEN1><ULTBEN2>CHEKANI T SAKALA 192 OCT 2011</ULTBEN2><DRCRIND>P</DRCRIND>" +
                "</Contract-Settlement></Settlements></Contract-Details-Full></FCUBS_BODY></CREATECONTRACT_FSFS_REQ>" +
                "</env:Body></env:Envelope>";

        System.out.println( stringXML);

        stringXML = XMLUtil.toXMLFormat( stringXML);
        System.out.println( stringXML);

    }
}