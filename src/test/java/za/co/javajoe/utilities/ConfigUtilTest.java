package za.co.javajoe.utilities;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import za.co.javajoe.confiurations.AmountConfigs;
import za.co.javajoe.domain.AgentDetails;

import java.util.List;
import java.util.Properties;

import static org.junit.Assert.*;

public class ConfigUtilTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testLoadConfigurations() throws Exception {

        assertNotNull("Your Configurations (Object) is NULL", ConfigUtil.getAmountConfigs());
        AmountConfigs amountConfigs = ConfigUtil.getAmountConfigs();

        assertNotNull( "Min Amount (Config) Is NULL", amountConfigs.getMinimumAmount());
        assertNotNull( "Max Amount (Config) Is NULL", amountConfigs.getMaximumAmount());

    }

    @Test
    public void testLoadConfigurationsByName() throws Exception {

        Properties properties = ConfigUtil.loadConfigurations("configs.properties");
        assertNotNull( properties);

        exception.expect( RuntimeException.class);
        ConfigUtil.loadConfigurations("");

        exception.expect( NullPointerException.class);
        exception.expectMessage("File Name Is NULL (EMPTY)");
        ConfigUtil.loadConfigurations( null);

    }

    @Test
    public void testLoadExcelFile() throws Exception {

        HSSFSheet worksheet =  ConfigUtil.loadExcelSheet();
        assertNotNull( worksheet);

    }


    @Test
    public void testmapExacelToObject() throws Exception {

        HSSFSheet worksheet =  ConfigUtil.loadExcelSheet();
        assertNotNull( worksheet);
        List<AgentDetails> collection = JoeUtil.mapExacelToObject(worksheet);
        assertNotNull( collection);


    }
}