package za.co.javajoe.arrays;

import org.junit.Before;
import org.junit.Test;
import za.co.javajoe.model.Person;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

/**
 * Created with IntelliJ IDEA.
 * User: letladi
 * Date: 2014/06/30
 * Time: 7:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class JustArraysExtendedTest {

    JustArraysExtended  justArraysExtended = new JustArraysExtended();

    @Before
    public void setUp() throws Exception {
        JustArraysExtended  justArraysExtended = new JustArraysExtended();
    }


   @Test
    public void testGetPersonNameByIndex() throws Exception {

        Person tempPerson=justArraysExtended.getPersonObjectNameByIndex(1);
        String name =tempPerson.getName();

        System.out.println(name);

        assertThat(tempPerson, notNullValue());
        assertThat(justArraysExtended.getPersonObjectNameByIndex(1), equalTo(tempPerson));

    }

   @Test
    public void testgetPeopleObjectNames() throws Exception{

        assertNotNull("List (IsEmtyDud) Is NULL", justArraysExtended.getPeopleObjectNames());
        System.out.println("The size of the ListIs is: "+ justArraysExtended.getPeopleObjectNames().size());


    }

    @Test
    public void intializeNamesIntoList(){
        assertThat(justArraysExtended, notNullValue());
        justArraysExtended.printArrayContent();



    }
}
