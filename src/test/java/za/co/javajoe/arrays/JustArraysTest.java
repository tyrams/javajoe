package za.co.javajoe.arrays;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import static org.hamcrest.MatcherAssert.assertThat;

/**
 * AUTHOR: Thabo Matjuda
 * DATE: 29 June 2014
 * DESCRIPTION: Testing (JustArrays)
 */

public class JustArraysTest {

    JustArrays justArrays;

    @Before
    public void setUp() throws Exception {
        justArrays = new JustArrays();
    }


    @Test
    public void testGetPersonNameByIndex() throws Exception {

        assertThat( justArrays, notNullValue());
        assertThat( justArrays.getPersonNameByIndex(1), equalTo("Tlou"));

    }

    @Test
    public void testPrintArrayContent() throws Exception {

        assertThat( justArrays, notNullValue());
        justArrays.printArrayContent();

    }

    @Test
    public void testGetPeopleNames() throws Exception {

        assertNotNull("Array (getPeopleNames()) Is NULL", justArrays.getPeopleNames());
        System.out.println("The size of the array is: "+ justArrays.getPeopleNames().length);

    }

    @Test
    public void testConvertArrayToList() throws Exception {

        assertThat( justArrays.convertArrayToList(), notNullValue());

        List<String> peopleNamesList = justArrays.convertArrayToList();
        assertNotNull( peopleNamesList);

    }
}